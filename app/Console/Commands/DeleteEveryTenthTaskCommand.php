<?php

namespace App\Console\Commands;

use App\Jobs\DeleteEveryTenthTaskJob;
use Illuminate\Console\Command;

class DeleteEveryTenthTaskCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:every-tenth-task-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DeleteEveryTenthTaskJob::dispatch();
    }
}
